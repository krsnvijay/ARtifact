import { makeExecutableSchema } from 'graphql-tools';
import RootQuery from './queries';
import Types from './types/index';
import Resolvers from './resolvers';

const SchemaDefinition = `
  schema {
    query: Query
  }
`;

export default makeExecutableSchema({
  typeDefs: [
    SchemaDefinition, RootQuery,
    ...Types,
  ],
  resolvers: Resolvers,
});
