import Dish from '../models/dish';
import Menu from '../models/menu';
import Restaurant from '../models/restaurant';

const resolvers = {
  Query: {
    restaurants: async () => Restaurant.find(),
    restaurant: async (obj, { id }) => Restaurant.findById(id),
    menus: async (obj, { id }) => Menu.find({
      restaurantId,
    }),
    menu: async (obj, { id }) => Menu.findById(id),
    dishes: async (obj, { menuId }) => Dish.find({menuId}),
    dish: async (obj, { id }) => Dish.findById(id),
  },
  Restaurant: {
    menus: async restaurant => Menu.find({
      restaurantId: restaurant._id,
    }),
  },
  Menu: {
    dishes: async menu => Dish.find({
      menuId: menu._id,
    }),
  },
};
export default resolvers;
