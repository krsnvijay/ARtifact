const dishType = `
"""
Dish Type
"""
type Dish {
    """
    id of the dish
    """
    id: ID!
    """
    id of the menu
    """
    menuId:ID!
    """
    name of the dish
    """
    name: String!
    """
    image of the dish
    """
    image: String!
    """
    description of the dish
    """
    description: String!
    """
    url to 3d model of the dish (glTF)
    """
    model: String!
    """
    price of the dish
    """
    price: Float!
}
`;
export default dishType;
