import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import Menu from './menu';

const Restaurant = ({ restaurant, className }) => (
  <div className={`card ${className || ''}`} key={restaurant.id}>
    <img src={restaurant.image} alt={restaurant.name} />
    <div className="card-content">
      <header>
        <strong>
          <a href={`/restaurants/${restaurant.id}`}>{restaurant.name}</a>
        </strong>
      </header>
      {restaurant.description}
      <address>
        {`${restaurant.address.building}, ${restaurant.address.area}, ${
          restaurant.address.state
        }, ${restaurant.address.pincode}`}
      </address>
    </div>
  </div>
);
export default Restaurant;

const RestaurantDetail = ({ match: { params: { restaurantId } } }) => (
  <Query
    query={gql`
      query getRestaurantById($restaurantId: ID!) {
        restaurant(id: $restaurantId) {
          id
          name
          image
          description
          address {
            building
            area
            state
            pincode
          }
          menus {
            id
            name
            image
            description
          }
        }
      }
    `}
    variables={{ restaurantId }}
  >
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Error :(</p>;

      return (
        <div>
          <Restaurant
            key={data.restaurant.id}
            restaurant={data.restaurant}
            className="card-detail"
          />
          <h1>Menus</h1>
          <div className="list">
            {data.restaurant.menus.map(menu => (
              <Menu key={menu.id} menu={menu} />
            ))}
          </div>
        </div>
      );
    }}
  </Query>
);
export { RestaurantDetail };
