const addressType = `
"""
Address Type - User or Restaurant
"""
type Address {
    """
    building's location
    """
    building: String!
    """
    area of the location
    """
    area: String!
    """
    state of the location
    """
    state: String!
    """
    pincode of the location
    """
    pincode: String!
}
`;
export default addressType;
