import faker from 'faker';
import Dish from './models/dish';
import Menu from './models/menu';
import Restaurant from './models/restaurant';

export async function clearDB() {
  try {
    await Restaurant.remove();
    await Menu.remove();
    await Dish.remove();
    console.log('DB Cleared');
  } catch (err) {
    console.log(err);
  }
}
export async function seedDB() {
  const restaurant = new Restaurant({
    name: faker.company.companyName(),
    image: 'https://lorempixel.com/400/200/food/',
    description: faker.company.catchPhrase(),
    address: {
      building: faker.address.streetPrefix(),
      area: faker.address.streetName(),
      city: faker.address.city(),
      state: faker.address.state(),
      pincode: faker.address.zipCode(),
    },

  });
  const menu = new Menu({
    name: faker.commerce.department(),
    image: faker.image.food(),
    description: faker.commerce.productAdjective(),
  });
  const dish = new Dish({
    name: faker.commerce.productName(),
    image: faker.image.food(),
    description: faker.commerce.productMaterial(),
    model: faker.internet.url(),
    price: faker.commerce.price(),
  });
  try {
    const newRestaurant = await restaurant.save();
    menu.restaurantId = newRestaurant._id;
    const newMenu = await menu.save();
    dish.menuId = newMenu._id;
    const newDish = await dish.save();
    console.log(JSON.stringify(newRestaurant, null, 2));
    console.log(JSON.stringify(newMenu, null, 2));
    console.log(JSON.stringify(newDish, null, 2));
  } catch (err) {
    console.log(err);
  }
}
export default async function populateDB(items) {
  await clearDB();
  for (let i = 0; i < items; i += 1) {
    seedDB();
  }
}
