import dishType from './dish.type';
import addressType from './address.type';
import menuType from './menu.type';
import restaurantType from './restaurant.type';

export default [restaurantType, dishType, menuType, addressType];
