const restaurantType = `
"""
Restaurant Type
"""
type Restaurant {
    """
    Unique id of The Restaurant
    """
    id: ID!
    """
    name of The Restaurant
    """
    name: String!
    """
    image of The Restaurant
    """
    image: String!
    """
    description of The Restaurant
    """
    description: String!
    """
    menu of The Restaurant
    """
    menus: [Menu!]
    """
    address of The Restaurant
    """
    address: Address!
}
`;
export default restaurantType;
