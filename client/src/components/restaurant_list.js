import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import Restaurant from './restaurant';

const restaurants = () => (
  <div className="list">
    <Query
      query={gql`
        {
          restaurants {
            id
            name
            image
            description
            address {
              building
              area
              state
              pincode
            }
          }
        }
      `}
    >
      {({ loading, error, data }) => {
        if (loading) return <p>Loading...</p>;
        if (error) return <p>Error :(</p>;

        return data.restaurants.map(restaurant => (
          <Restaurant key={restaurant.id} restaurant={restaurant} />
        ));
      }}
    </Query>
  </div>
);
export default restaurants;
