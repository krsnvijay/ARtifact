import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import Dish from './dish';

const Menu = ({ menu, className }) => (
  <div className={`card ${className || ''}`} key={menu.id}>
    <img src={menu.image} alt={menu.name} />
    <div className="card-content">
      <header>
        <strong>
          <a href={`/menus/${menu.id}`}>{menu.name}</a>
        </strong>
      </header>
      {menu.description}
    </div>
  </div>
);
export default Menu;

const MenuDetail = ({ match: { params: { menuId } } }) => (
  <Query
    query={gql`
      query getMenuById($menuId: ID!) {
        menu(id: $menuId) {
          id
          name
          image
          description
          dishes {
            id
            image
            description
            name
            price
            model
          }
        }
      }
    `}
    variables={{ menuId }}
  >
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Error :(</p>;

      return (
        <div>
          <Menu key={data.menu.id} menu={data.menu} className="card-detail" />
          <h1>Dishes</h1>
          <div className="list">
            {data.menu.dishes.map(dish => <Dish key={dish.id} dish={dish} />)}
          </div>
        </div>
      );
    }}
  </Query>
);
export { MenuDetail };
