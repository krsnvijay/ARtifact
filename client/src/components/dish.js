import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { Helmet } from 'react-helmet';

// import { Entity, Scene } from 'aframe-react';

const Dish = ({ dish, className }) => (
  <div className={`card ${className || ''}`} key={dish.id}>
    <img src={dish.image} alt={dish.name} />
    <div className="card-content">
      <header>
        <strong>
          <a href={`https://artifact.netlify.com?model=${dish.model}`}>
            `${dish.name}-${dish.price}`
          </a>
        </strong>
      </header>
      {dish.description}
      <button>Order</button>
    </div>
  </div>
);
export default Dish;
function createMarkup(model) {
  return {
    __html: `
    <a-scene embedded arjs>
    <a-assets>
        <a-asset-item id="dish" src="${model}"></a-asset-item>
    </a-assets>
    <a-marker preset="hiro">
        <a-entity gltf-model="#dish" scale="0.10 0.10 0.10"></a-entity>
    </a-marker>
    <a-entity camera></a-entity>
</a-scene>`
  };
}
const DishDetail = ({ match: { params: { dishId } } }) => (
  <Query
    query={gql`
      query getDishById($dishId: ID!) {
        dish(id: $dishId) {
          id
          image
          description
          name
          price
          model
        }
      }
    `}
    variables={{ dishId }}
  >
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Error :(</p>;

      return (
        <div>
          <Helmet>
            <script src="https://aframe.io/releases/0.8.0/aframe.min.js" />
            <script src="https://cdn.rawgit.com/jeromeetienne/AR.js/1.5.5/aframe/build/aframe-ar.js" />
          </Helmet>
          <div dangerouslySetInnerHTML={createMarkup(data.dish.model)} />
        </div>
      );
    }}
  </Query>
);
export { DishDetail };
