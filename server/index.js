import dotenv from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { ApolloEngine } from 'apollo-engine';
import cors from 'cors';
import populateDB from './seed';
import schema from './graphql/schema';


dotenv.config();
// connect to the mongodb hosted locally
mongoose.connect(process.env.MONGO_URI);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  // we're connected!
  //populateDB(10);
});
// clearDB and Populate

// Initialize the app
const app = express();
app.use(cors());
app.options('*', cors());
// The GraphQL endpoint
app.use('/graphql', bodyParser.json(), graphqlExpress({
  schema,
  tracing: true,
  cacheControl: true,
}));

// GraphiQL, a visual editor for queries
app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
  tracing: true,
  cacheControl: true,
}));

const engine = new ApolloEngine({
  apiKey: process.env.API_KEY,
});
// Start your server
engine.listen({
  port: process.env.PORT || 3000,
  expressApp: app,
});
