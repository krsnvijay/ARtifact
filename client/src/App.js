import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Restaurants from './components/restaurant_list';
import { RestaurantDetail } from './components/restaurant';
import { MenuDetail } from './components/menu';
import { DishDetail } from './components/dish';
import Login from './components/login';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to ARtifact</h1>
          <p>AR Restaurant Menu</p>
        </header>
        <Router>
          <Switch>
            <Route path="/" exact component={Restaurants} />
            <Route
              path="/restaurants/:restaurantId"
              exact
              component={RestaurantDetail}
            />
            <Route path="/menus/:menuId" component={MenuDetail} />
            <Route path="/dishes/:dishId" component={DishDetail} />
            <Route path="/login" component={Login} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
