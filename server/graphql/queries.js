const rootQuery =
    `
"""
Root Query
"""
type Query {
    """
    List of restaurants
    """
    restaurants: [Restaurant!]
    """
    Get restaurant by id
    """
    restaurant(id: ID!): Restaurant!
    """
    List of menus in a restaurant
    """
    menus(id: ID!): [Menu!]
    """
    Get menu by id
    """
    menu(id: ID!): Menu!
    """
    List of dishes in a menu
    """
    dishes(id: ID!): [Dish!]
    """
    Get Dish by id
    """
    dish(id: ID!): Dish!
}`;
export default rootQuery;
