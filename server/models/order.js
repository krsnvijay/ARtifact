import mongoose from 'mongoose';

const orderSchema = new mongoose.Schema({
  created: {
    type: Date,
    default: Date.now,
  },
  status: {
    type: String,
    enum: ['Pending', 'Delivered', 'Cancelled'],
  },
  amount: {
    type: Number,
    required: true,
  },
  dishes: [{
    quantity: {
      type: Number,
      required: true,
    },
    dishId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Dish',
    },
  }],
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
});
export default mongoose.model('Order', orderSchema);
