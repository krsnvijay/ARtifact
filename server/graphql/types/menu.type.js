const menuType = `
"""
Menu Type
"""
type Menu {
    """
    unique id of the menu
    """
    id: ID!
    """
    id of the restaurant
    """
    restaurantId:ID!
    """
    name/category of the menu
    """
    name: String!
    """
    image of the menu
    """
    image: String!
    """
    description of menu
    """
    description: String!
    """
    dishes of the menu
    """
    dishes:[Dish!]

}
`;
export default menuType;
