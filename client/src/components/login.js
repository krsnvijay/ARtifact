import React from 'react';

const Login = ({ login }) => (
  <form action="/" method="get" className="login">
    Login
    <input type="text" id="username" placeholder="username" required />
    <input type="password" id="password" placeholder="password" required />
    <input type="submit" value="Login" />
  </form>
);
export default Login;
